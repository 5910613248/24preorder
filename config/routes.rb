Rails.application.routes.draw do

  get '/buy_shoe',to: 'buy#index'
  get '/buy_purse',to: 'buy#index1'
  get '/buy_hood',to: 'buy#index2'
  get '/buy_hood1',to: 'buy#index3'
  get 'homelogged_in/index'
  get 'detail_logged/index'
  get '/detail_no_login', to: 'detail_no_login#index'
  get '/detail_no_login2', to: 'detail_no_login#index2'
  get '/detail_no_login3', to: 'detail_no_login#index3'
  get '/detail_no_login4', to: 'detail_no_login#index4'
  get '/detail_logged', to: 'detail_logged#index'
  get '/detail_logged2', to: 'detail_logged#index2'
  get '/detail_logged3', to: 'detail_logged#index3'
  get '/detail_logged4', to: 'detail_logged#index4'
  get '/welcome', to: 'welcome#index'
  get '/homelogged_in', to: 'homelogged_in#index'
  get '/register', to: 'register#index'



  resources :orders
  get 'logout', to: 'sessions#destroy', as: 'logout'
  resources :users
  resources :sessions, only: [:new, :create, :destroy]
  get '/signup', to: 'users#new'
  get '/login', to: 'sessions#new'
  get '/home',  to: 'users#home'
  get '/about', to: 'users#about'
  get '/userorder', to: 'orders#userorder'
  get '/neworder', to: 'orders#new'
  get '/neworderfalse', to: 'sessions#new'
  root 'users#home'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
