require 'rails_helper'

RSpec.describe Order, type: :model do
    fixtures :order
    before :each do
        @order_test = order(:car)
    end
  it "is valid with valid attributes" do
        expect(@order_test).to be_valid
    end
    
    it "is not valid without a user_id" do 
        @order_test.name = nil
        expect(@order_test).to_not be_valid
    end
    
    it "is not valid without a item_name" do 
        @order_test.productname = nil
        expect(@order_test).to_not be_valid
    end
    
    it "is not valid without a detail" do 
        @order_test.address = nil
        expect(@order_test).to_not be_valid
    end
    
    
end