require 'rails_helper'

RSpec.describe "orders/index", type: :view do
  before(:each) do
    assign(:orders, [
      Order.create!(
        :name => "Name",
        :productname => "Productname",
        :address => "Address"
      ),
      Order.create!(
        :name => "Name",
        :productname => "Productname",
        :address => "Address"
      )
    ])
  end

  it "renders a list of orders" do
    render
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "Productname".to_s, :count => 2
    assert_select "tr>td", :text => "Address".to_s, :count => 2
  end
end
