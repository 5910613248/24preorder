require "application_system_test_case"

class MyUsersTest < ApplicationSystemTestCase
  setup do
    @my_user = my_users(:one)
  end

  test "visiting the index" do
    visit my_users_url
    assert_selector "h1", text: "My Users"
  end

  test "creating a My user" do
    visit my_users_url
    click_on "New My User"

    fill_in "Email", with: @my_user.email
    fill_in "Password", with: 'secret'
    fill_in "Password confirmation", with: 'secret'
    click_on "Create My user"

    assert_text "My user was successfully created"
    click_on "Back"
  end

  test "updating a My user" do
    visit my_users_url
    click_on "Edit", match: :first

    fill_in "Email", with: @my_user.email
    fill_in "Password", with: 'secret'
    fill_in "Password confirmation", with: 'secret'
    click_on "Update My user"

    assert_text "My user was successfully updated"
    click_on "Back"
  end

  test "destroying a My user" do
    visit my_users_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "My user was successfully destroyed"
  end
end
